# -*- coding: utf-8 -*-
from irc3.plugins.command import command
import irc3
from struct import *
from string import Template
import socket, sys, time, datetime
from urllib.parse import urlsplit

@irc3.plugin
class Emotes:

    def __init__(self, bot):
        self.bot = bot

    @command(permission='view')
    def lenny(self, mask, target, args):
        """Wysyła emotikonę "lenny"

            %%lenny
        """

        yield "( ͡° ͜ʖ ͡°)"

    @command(permission='view')
    def lenonki(self, mask, target, args):
        """Wysyła emotikonę "lenonki"

            %%lenonki
        """

        yield "(⌐● ͜ ●)"
